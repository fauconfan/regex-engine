#!/bin/bash

set -eux

apt-get update
apt-get upgrade -y

apt-get install -y "$@"