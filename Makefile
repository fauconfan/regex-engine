
NAME = regex_tester

_RED=$(shell tput setaf 1 2> /dev/null || echo "")
_GREEN=$(shell tput setaf 2 2> /dev/null || echo "")
_YELLOW=$(shell tput setaf 3 2> /dev/null || echo "")
_BLUE=$(shell tput setaf 4 2> /dev/null || echo "")
_PURPLE=$(shell tput setaf 5 2> /dev/null || echo "")
_CYAN=$(shell tput setaf 6 2> /dev/null || echo "")
_WHITE=$(shell tput setaf 7 2> /dev/null || echo "")
_END=$(shell tput sgr0 2> /dev/null || echo "")

MAKEFLAGS += --no-print-directory

CC = clang

CFLAGS = -Wall -Wextra -Werror -Weverything
IFLAGS = -I $(INC_FOLDER)
SRC_CFLAGS =
BIN_CFLAGS =
LFLAGS =
SRC_FLAGS = $(SRC_CFLAGS) $(CFLAGS) $(IFLAGS)
BIN_FLAGS = $(BIN_CFLAGS) $(CFLAGS) $(IFLAGS)

## Define SRC, OBJ, and INC
include files.makefile

########## COMPILATION ##########

.PHONY: all
all: $(NAME)

$(NAME): $(OBJ)
	@printf "\\t%sLD%s\\t" "$(_CYAN)" "$(_END)"
	@$(CC) $(BIN_FLAGS) $^ -o $@ $(LFLAGS)
	@printf "%s\\n" "$@"

%.o: %.c $(INC)
	@printf "\\t%sCC%s\\t" "$(_GREEN)" "$(_END)"
	@$(CC) $(SRC_FLAGS) -c $< -o $@
	@printf "%s\\n" "$<"

.PHONY: clean
clean:
	@rm -rf $(OBJ)
	@echo "Objects removed"

.PHONY: fclean
fclean: clean
	@rm -rf $(NAME)
	@echo "Executable $(NAME) removed"

.PHONY: re
re:
	@make fclean
	@make all

########## TEST ##########

##### External Linters #####

.PHONY: clang-tidy
clang-tidy:
	@clang-tidy \
		-checks="*" \
		-header-filter="*" \
		-warnings-as-errors="*" \
		$(SRC) -- -I $(INC_FOLDER)

.PHONY: cppcheck
cppcheck:
	@cppcheck \
		--error-exitcode=1 \
		--enable=all \
		--check-config \
		-I $(INC_FOLDER) $(SRC)

.PHONY: cpplint
cpplint:
	@cpplint \
		--filter="-legal,-whitespace,-build/include_subdir,-build/header_guard,-build/include_what_you_use,-readability/casting" \
		$(SRC) $(INC)

.PHONY: lint
lint: clang-tidy cppcheck cpplint

##### CHECK #####

TESTS_DIR = tests/
