
SRC_FOLDER = src/
INC_FOLDER = inc/

_SRC = \
	main.c \
	main_custom.c \
	regex.c \
	test_handler.c \

_SRC_TESTS = \
	concat.c \
	dot.c \
	group.c \
	line.c \
	list.c \
	optional.c \
	plus.c \
	repetition.c \
	star.c \
	union.c \

_SRC_T_REGEX = \

_SRC_T_REGEX_ALLOC = \
	t_regex.c \
	t_regex_brut.c \
	t_regex_list.c \

_SRC_T_REGEX_PRINT = \
	t_regex.c \
	t_regex_brut.c \
	t_regex_list.c \

SRC = \
		$(addprefix $(SRC_FOLDER), $(_SRC)) \
		$(addprefix $(SRC_FOLDER)tests/, $(_SRC_TESTS)) \
		$(addprefix $(SRC_FOLDER)t_regex/, $(_SRC_T_REGEX)) \
		$(addprefix $(SRC_FOLDER)t_regex/alloc/, $(_SRC_T_REGEX_ALLOC)) \
		$(addprefix $(SRC_FOLDER)t_regex/print/, $(_SRC_T_REGEX_PRINT))

OBJ = $(SRC:%.c=%.o)

_INC = \
	regex.h \
	t_regex.h \
	tests.h \
	tests_spec.h \

INC = $(addprefix $(INC_FOLDER), $(_INC))
