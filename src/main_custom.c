#include "regex.h"
#include "t_regex.h"

void	main_custom(void) {
	t_regex		*re = NULL;

	// Re structure expected for pattern "abc[cdef]fghij"
	re = (t_regex *)re_alloc_brut("abc", 3);
	re->next = (t_regex *)re_alloc_list("cdef", 4, FALSE);
	re->next->next = (t_regex *)re_alloc_brut("fghij", 5);

	re_print(re);

	re_free(re);
}
