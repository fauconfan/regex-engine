
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "tests_spec.h"
#include "tests.h"

#include "regex.h"

typedef struct		s_test_function {
	const char		*name;
	void			(*f)(void);
}					t_test_function;

static const t_test_function	table[] = {
	{ "all", NULL },
	{ "concat", concat_f },
	{ "dot", dot_f },
	{ "group", group_f },
	{ "line", line_f },
	{ "list", list_f },
	{ "optional", optional_f },
	{ "plus", plus_f },
	{ "repetition", repetition_f },
	{ "star", star_f },
	{ "union", union_f }
};

void				treat_arg(const char *arg) {
	t_bool		found = FALSE;
	t_bool		runAll = FALSE;

	for (size_t i = 0; i < SIZE_ARRAY(table); i++) {
		if (strcmp(table[i].name, arg) == 0) {
			if (table[i].f != NULL) {
				table[i].f();
			} else {
				runAll = TRUE;
			}
			found = TRUE;
			break;
		}
	}

	if (found == FALSE) {
		printf("Should specify at least one valid argument");
		assert(FALSE);
	}

	if (runAll) {
		for (size_t i = 0; i < SIZE_ARRAY(table); i++) {
			if (table[i].f != NULL) {
				table[i].f();
			}
		}
	}
}
