
#include <assert.h>

#include "regex.h"
#include "tests_spec.h"

void		optional_f(void) {
	assert(regex("b?", ""));
	assert(regex("b?", "b"));
	assert(regex("b?", "bb"));
	assert(regex("b?", "a"));

	assert(regex("ab?", "a"));
	assert(regex("ab?", "ab"));
	assert(regex("ab?", "abb"));
}
