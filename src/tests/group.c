
#include <assert.h>

#include "regex.h"
#include "tests_spec.h"

void		group_f(void) {
	assert(regex("(abc)", "abc"));
	assert(regex("(abc)*", "abcabcabc"));
}
