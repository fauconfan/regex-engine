
#include <assert.h>

#include "regex.h"
#include "tests_spec.h"

void		line_f(void) {
	assert(regex("Robert", "Je m'appelle Robert"));
	assert(regex("Bonjour", "Bonjour, un ptit café ?"));

	// Début
	assert(regex("^Bonjour", "Bonjour le monde"));
	assert(regex("^Bonjour", "...Bonjour le monde") == 0);
	// Fin
	assert(regex("Au revoir$", "...Au revoir"));
	assert(regex("Au revoir$", "Au revoir Patrick") == 0);
}
