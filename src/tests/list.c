
#include <assert.h>

#include "regex.h"
#include "tests_spec.h"

void		list_f(void) {
	assert(regex("[abcde]", "d"));
	assert(regex("[abcde]", "e"));
	assert(regex("[abcde]", "f") == FALSE);

	assert(regex("[^abcde]", "d") == FALSE);
	assert(regex("[^abcde]", "e") == FALSE);
	assert(regex("[^abcde]", "f"));
}
