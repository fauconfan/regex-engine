
#include <assert.h>

#include "regex.h"
#include "tests_spec.h"

void		star_f(void) {
	assert(regex("b*", "bbbbb"));
	assert(regex("b*", "") == FALSE);
}
