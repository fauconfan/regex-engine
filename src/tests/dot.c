
#include <assert.h>

#include "regex.h"
#include "tests_spec.h"

void		dot_f(void) {
	assert(regex(".", "b"));
	assert(regex(".", "c"));
	assert(regex(".", "") == FALSE);
}
