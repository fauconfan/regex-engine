
#include <assert.h>

#include "regex.h"
#include "tests_spec.h"

void		union_f(void) {
	assert(regex("ab|cd", "ab"));
	assert(regex("ab|cd", "cd"));
	assert(regex("ab|cd", "ef") == FALSE);
	assert(regex("ab|cd|ef", "ef"));
}
