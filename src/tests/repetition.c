
#include <assert.h>

#include "regex.h"
#include "tests_spec.h"

void		repetition_f(void) {
	assert(regex("ca{3}c", "caaac"));
	assert(regex("ca{3}c", "caac") == FALSE);
	assert(regex("ca{3}c", "caaaac") == FALSE);

	assert(regex("ca{1,3}c", "caaac"));
	assert(regex("ca{1,3}c", "caac"));
	assert(regex("ca{1,}c", "caaac"));
	assert(regex("ca{1,}c", "caaaac"));
	assert(regex("ca{,3}c", "caaac"));
	assert(regex("ca{,3}c", "caac"));
}
