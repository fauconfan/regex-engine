
#include <assert.h>

#include "regex.h"
#include "tests_spec.h"

void		concat_f(void) {
	assert(regex("ab", "ab"));
	assert(regex("abc", "abc"));
	assert(regex("abc", "ab") == FALSE);
}
