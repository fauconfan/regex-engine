#include <stdio.h>
#include <string.h>

#include "regex.h"
#include "tests.h"

static void	display_help(void) {
	printf("No help is available for now you need to know the program.\n");
}

int			main(int argc, char **argv) {
	int		retcode = 1;

	if (argc != 2 || strcmp(argv[1], "--help") == 0) {
		display_help();
	} else if (strcmp(argv[1], "custom") == 0) {
		main_custom();
		retcode = 0;
	} else {
		treat_arg(argv[1]);
		retcode = 0;
	}
	return (retcode);
}
