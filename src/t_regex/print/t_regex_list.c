#include "t_regex.h"

void		re_list_print(t_regex_list *re) {
	printf("- LIST\n");
	printf("    list: '%.*s'\n", (int)re->pattern_len, re->pattern);
	printf("    reverse: %s\n", re->reverse ? "TRUE": "FALSE");
}
