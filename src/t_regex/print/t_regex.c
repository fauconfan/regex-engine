#include "t_regex.h"

void	re_print(t_regex *re) {
	switch(re->type) {
		case RE_BRUT:
			re_brut_print((t_regex_brut *)re);
			break;
		case RE_LIST:
			re_list_print((t_regex_list *)re);
			break;
	}

	if (re->next != NULL) {
		re_print(re->next);
	}
}
