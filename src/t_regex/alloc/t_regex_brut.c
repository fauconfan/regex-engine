#include "t_regex.h"

t_regex_brut		*re_alloc_brut(const char *pattern, size_t pattern_len) {
	t_regex_brut	*res;

	res = (t_regex_brut *)malloc(sizeof(t_regex_brut));
	if (res == NULL) {
		return NULL;
	}
	res->next = NULL;
	res->type = RE_BRUT;
	res->pattern = pattern;
	res->pattern_len = pattern_len;
	return (res);
}

void				re_free_brut(t_regex_brut *re) {
	free(re);
}
