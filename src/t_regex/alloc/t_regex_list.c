#include "t_regex.h"

t_regex_list		*re_alloc_list(const char *pattern, size_t pattern_len, t_bool reverse) {
	t_regex_list	*res;

	res = (t_regex_list *)malloc(sizeof(t_regex_list));
	if (res == NULL) {
		return NULL;
	}
	res->next = NULL;
	res->type = RE_LIST;
	res->pattern = pattern;
	res->pattern_len = pattern_len;
	res->reverse = reverse;
	return (res);
}

void				re_free_list(t_regex_list *re) {
	free(re);
}
