#include "t_regex.h"

void		re_free(t_regex *re) {
	if (re->next != NULL) {
		re_free(re->next);
		re->next = NULL;
	}

	switch(re->type) {
		case RE_BRUT:
			re_free_brut((t_regex_brut *)re);
			break;
		case RE_LIST:
			re_free_list((t_regex_list *)re);
			break;
	}
}
