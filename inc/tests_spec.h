#ifndef TESTS_SPEC_H
# define TESTS_SPEC_H

void				concat_f(void);
void				dot_f(void);
void				group_f(void);
void				line_f(void);
void				list_f(void);
void				optional_f(void);
void				plus_f(void);
void				repetition_f(void);
void				star_f(void);
void				union_f(void);

#endif
