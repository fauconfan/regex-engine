#ifndef REGEX_T_H
# define REGEX_T_H

# include <stdlib.h>
# include <stdio.h>

# include "regex.h"

typedef enum		e_regex_type {
	RE_BRUT,
	RE_LIST
	// RE_GROUP,
	// RE_UNION
}					t_regex_type;

typedef struct		s_regex_quantifier {
	t_bool			has;
	unsigned char	min;
	unsigned char	max;
	char			_empty[1]; // 8 bytes alignment
}					t_regex_quantifier;

# define REGEX_CORE_ATTR \
	struct s_regex		*next; \
	t_regex_type		type; \
	t_regex_quantifier	quantifier;

typedef struct		s_regex {
	REGEX_CORE_ATTR
}					t_regex;

typedef struct		s_regex_brut {
	REGEX_CORE_ATTR
	const char		*pattern;
	size_t			pattern_len;
}					t_regex_brut;

typedef struct		s_regex_list
{
	REGEX_CORE_ATTR
	const char		*pattern;
	size_t			pattern_len;
	t_bool			reverse;
	char			_empty[7]; // 8 bytes alignment
}					t_regex_list;

// typedef struct		s_regex_group
// {
// 	REGEX_CORE_ATTR
// 	t_regex			*group;
// }					t_regex_group;

// typedef struct		s_regex_union
// {
// 	REGEX_CORE_ATTR
// 	t_regex			*list_union;
// 	size_t			union_length;
// }					t_regex_union;

# undef REGEX_CORE_ATTR

// ALLOC

void				re_free(t_regex *re);

t_regex_brut		*re_alloc_brut(const char *pattern, size_t pattern_len);
void				re_free_brut(t_regex_brut *re);

t_regex_list		*re_alloc_list(const char *pattern, size_t pattern_len, t_bool reverse);
void				re_free_list(t_regex_list *re);

// PRINT

void				re_print(t_regex *re);

void				re_brut_print(t_regex_brut *re);
void				re_list_print(t_regex_list *re);

#endif
