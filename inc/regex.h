#ifndef REGEX_H
# define REGEX_H

# include <string.h>

typedef unsigned char		t_bool;

# define TRUE	1
# define FALSE	0

# define SIZE_ARRAY(arr)		(sizeof(arr) / sizeof(*arr))

t_bool			regex(const char *pattern, const char *string);

void			main_custom(void);

#endif
